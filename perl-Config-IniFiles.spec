%define mod_name Config-IniFiles
Name:           perl-%{mod_name}
Version:        3.000003
Release:        3
Summary:        A module for reading .ini-style configuration files
# LICENSE:                              GPL+ or Artistic
# lib/Config/IniFiles.pm:               GPL+ or Artistic
## Not distributed in a binary package
# t/30parameters-with-empty-values.t:   MIT
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/S/SH/SHLOMIF/%{mod_name}-%{version}.tar.gz
BuildRequires:  coreutils findutils make perl-interpreter perl-generators perl(Module::Build) >= 0.36
# Module::Build::Compat not used, we run Build.PL
BuildRequires:  perl(strict)
# Test::Run::CmdLine::Iface not used
BuildRequires:  perl(warnings) sed
# Run-time:
BuildRequires:  perl(Carp) perl(Fcntl) perl(File::Basename) perl(File::Temp) perl(IO::Scalar) >= 2.109 perl(List::Util) >= 1.33 perl(Symbol) perl(vars)
# Tests:
BuildRequires:  perl(base) perl(English) perl(Exporter) perl(File::Spec) perl(IO::File) perl(IO::Handle) perl(IPC::Open3) perl(lib) perl(parent) perl(Scalar::Util) perl(Test::More)
BuildArch:      noarch
# Not autodetected. Found in lib/Config/IniFiles.pm:2761
Requires:       perl(IO::Scalar) >= 2.109
# Also not autodetected
Requires:       perl(List::Util) >= 1.33

# Filter under-specified requires
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(List::MoreUtils\\)$

%description
Config::IniFiles is a module intended to make life easier for
those who rely on flat configuration files.

%prep
%autosetup -n %{mod_name}-%{version} -p1
# Normalize end-of-lines
sed -i -e 's/\r$//' Changes OLD-Changes.txt

%build
perl Build.PL installdirs=vendor
./Build

%install
./Build install destdir=%{buildroot} create_packlist=0
%{_fixperms} %{buildroot}/*

%check
./Build test

%files
%license LICENSE
%doc Changes OLD-Changes.txt README
%{perl_vendorlib}/Config/
%{_mandir}/man3/*.3pm*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 3.000003-3
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Oct 25 2022 huyubiao <huyubiao@huawei.com> - 3.000003-2
- define mod_name to opitomize the specfile

* Thu Jul 30 2020 wenzhanli<wenzhanli2@huawei.com> - 3.000003-1
- Type:NAE
- ID:NA
- SUG:NA
- DESC:update version 3.000003

* Wed Feb 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.98-4
- Package init
